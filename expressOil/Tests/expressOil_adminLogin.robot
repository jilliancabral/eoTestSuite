*** Settings ***
Documentation     Admin Login
Library           Selenium2Library
Resource          ../Resources/resources.robot

*** Variables ***
${VALID USERNAME}    admin@ezytire.com
${VALID PASSWORD}    notGreg1Camel
${EMPTY PASSWORD}    ${EMPTY}
${EMPTY USERNAME}    ${EMPTY}
${INVALID USERNAME}    admin
${INVALID PASSWORd}    admin

*** Test Cases ***
Valid Login Scenario
    [Documentation]    To validate valid login credentials
    Open Browser    https://www.expressoil.com/admin/    chrome
    Maximize Browser Window
    Input Username    ${VALID USERNAME}
    Input password    ${VALID PASSWORD}
    Submit Credentials
    Should Be Able to Login
    Close Browser

Empty Password Scenario
    [Documentation]    To validate empty password
    Open Browser    https://www.expressoil.com/admin/    chrome
    Maximize Browser Window
    Input Username    ${VALID USERNAME}
    Input password    ${EMPTY PASSWORD}
    Submit Credentials
    Should Not Be Able to Login

Empty Username Scenario
    [Documentation]    To validate empty username
    Input Username    ${EMPTY USERNAME}
    Input password    ${VALID PASSWORD}
    Submit Credentials
    Should Not Be Able to Login

Empty Username and Password Scenario
    [Documentation]    To validate empty username and password
    Input Username    ${EMPTY USERNAME}
    Input password    ${EMPTY PASSWORD}
    Submit Credentials
    Should Not Be Able to Login

Invalid Password Scenario
    [Documentation]    To validate invalid password
    Input Username    ${VALID USERNAME}
    Input password    ${INVALID PASSWORD}
    Submit Credentials
    Should Not Be Able to Login

Invalid Username Scenario
    [Documentation]    To validate invalid username
    Input Username    ${INVALID USERNAME}
    Input password    ${VALID PASSWORD}
    Submit Credentials
    Should Not Be Able to Login

Invalid Username and Password Scenario
    [Documentation]    To validate invalid username and password
    Input Username    ${INVALID USERNAME}
    Input password    ${INVALID PASSWORD}
    Submit Credentials
    Should Not Be Able to Login
    Close Browser
