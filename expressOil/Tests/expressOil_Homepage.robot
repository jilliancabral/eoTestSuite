*** Settings ***
Library           Selenium2Library
Library           Collections
Resource          ../Resources/resources.robot
Library           String

*** Variables ***
${option_value}    1
${rim_value}      1

*** Test Cases ***
Validate Valid Vehicle
    [Documentation]    To validate valid selected vehicle
    Set Selenium Speed    2
    Open Browser    https://www.expressoil.com    chrome
    Sleep    2s
    Maximize Browser Window
    ${year}=    Get List Items    //*[@id="m177-year"]
    ${eval_year}=    Evaluate    random.randint(1, 37)    random
    Select From List By Label    xpath=//select[@id="m177-year"]    @{year}[${eval_year}]
    Sleep    3s
    ${make}=    Get List Items    //*[@id="m177-make"]
    ${eval_make}=    Evaluate    random.randint(1, 42)    random
    Select From List By Label    xpath=//select[@id="m177-make"]    @{make}[${eval_make}]
    Sleep    2s
    ${model}=    Get List Items    //*[@id="m177-model"]
    Select From List By Label    xpath=//select[@id="m177-model"]    @{model}[${option_value}]
    Sleep    2s
    ${option}=    Get List Items    //*[@id="m177-option"]
    Select From List By Label    xpath=//select[@id="m177-option"]    @{option}[${option_value}]
    Sleep    2s
    Click Element    xpath=//input[@name="searchvehicle"]
    Sleep    10s
    Should Be Able to Search
    Close Browser

Validate Invalid Vehicle
    [Documentation]    To validate incomplete vehicle details
    Set Selenium Speed    2
    Open Browser    https://www.expressoil.com    chrome
    Sleep    3s
    Maximize Browser Window
    ${year}=    Get List Items    //*[@id="m177-year"]
    ${eval_year}=    Evaluate    random.randint(1, 37)    random
    Select From List By Label    xpath=//select[@id="m177-year"]    @{year}[${eval_year}]
    Sleep    3s
    Click Element    xpath=//input[@name="searchvehicle"]
    Sleep    5s
    Should Not Be Able to Search
    Close Browser

Validate Valid Vehicle with Zip Code
    [Documentation]    To validate valid vehicle seleted with zip code
    Set Selenium Speed    2
    Open Browser    https://www.expressoil.com    chrome
    Sleep    2s
    Maximize Browser Window
    ${year}=    Get List Items    //*[@id="m177-year"]
    ${eval_year}=    Evaluate    random.randint(1, 37)    random
    Select From List By Label    xpath=//select[@id="m177-year"]    @{year}[${eval_year}]
    Sleep    3s
    ${make}=    Get List Items    //*[@id="m177-make"]
    ${eval_make}=    Evaluate    random.randint(1, 42)    random
    Select From List By Label    xpath=//select[@id="m177-make"]    @{make}[${eval_make}]
    Sleep    3s
    ${model}=    Get List Items    //*[@id="m177-model"]
    Select From List By Label    xpath=//select[@id="m177-model"]    @{model}[${option_value}]
    Sleep    2s
    ${option}=    Get List Items    //*[@id="m177-option"]
    Select From List By Label    xpath=//select[@id="m177-option"]    @{option}[${option_value}]
    Sleep    2s
    Input Text    //input[@name="zip"]    12345
    Sleep    2s
    Click Element    xpath=//input[@name="searchvehicle"]
    Sleep    10s
    Should Be Able to Search
    Close Browser

Validate Valid Search by Tire Size
    [Documentation]    To validate valid search by tire size
    Set Selenium Speed    2
    Open Browser    https://www.expressoil.com    chrome
    Sleep    2s
    Maximize Browser Window
    Click Link    //a[@href="#m177-size"]
    ${width}=    Get List Items    //*[@id="m177-width"]
    ${eval_width}=    Evaluate    random.randint(1, 76)    random
    Select From List By Label    xpath=//select[@id="m177-width"]    @{width}[${eval_width}]
    sleep    2s
    ${aspect}=    Get List Items    //*[@id="m177-aspect"]
    ${eval_aspect}=    Evaluate    random.randint(1, 1)    random
    Select From List By Label    xpath=//select[@id="m177-aspect"]    @{aspect}[${eval_aspect}]
    sleep    4s
    ${rim}=    Get List Items    //*[@id="m177-rim"]
    Select From List By Label    xpath=//select[@id="m177-rim"]    @{rim}[${rim_value}]
    sleep    2s
    Click Element    xpath=//input[@name="searchsize"]
    Sleep    10s
    Should Be Able to Search
    Close Browser

Validate Invalid Search by Tire Size
    [Documentation]    To validate invalid search by tire size
    Set Selenium Speed    2
    Open Browser    https://www.expressoil.com    chrome
    Sleep    2s
    Maximize Browser Window
    Click Link    //a[@href="#m177-size"]
    ${width}=    Get List Items    //*[@id="m177-width"]
    ${eval_width}=    Evaluate    random.randint(1, 76)    random
    Select From List By Label    xpath=//select[@id="m177-width"]    @{width}[${eval_width}]
    sleep    2s
    Click Element    xpath=//input[@name="searchsize"]
    Sleep    10s
    Should Not Be Able to Search
    Close Browser

Validate Find A Location Near You
    [Documentation]    To validate find a location near you widget
    Set Selenium Speed    2
    Open Browser    https://www.expressoil.com    chrome
    Sleep    2s
    Maximize Browser Window
    ${random_zip}=    Generate Random String    5    1234567890
    Log    ${random_zip}
    Input Text    xpath=(//input[@name="zip"])[3]    ${random_zip}
    Click Element    //input[@class="btn \ btn-primary btn-form marg-right5"]
    Sleep    10s
    Should Be Able to Search
    Close Browser

Validate Header Buttons
    [Documentation]    To validate header buttons
    Set Selenium Speed    2
    Open Browser    https://www.expressoil.com    chrome
    Sleep    2s
    Maximize Browser Window
    Click Link    //a[@href="/schedule-appointment/"]
    Sleep    10s
    Should Redirect to Shedule Appoinment Page
    Go Back
    Sleep    2s
    Click Link    //a[@href="/careers/"]
    Sleep    10s
    Should Redirect to Careers Page
    Go Back
    Sleep    2s
    Click Link    //a[@href="/franchise/"]
    Sleep    10s
    Should Redirect to Franchise Page
    Go Back
    Sleep    2s
    Click Link    //a[@href="/cart/"]
    Sleep    10s
    Should Redirect to Cart Page
    Close Browser

Get All Links
    [Tags]    Links
    Open Browser    https://www.expressoil.com    chrome
    Maximize Browser Window
    Comment    Count Number Of Links on the Page
    ${AllLinksCount}=    Get Matching Xpath Count    //a
    Comment    Log links count
    Log    ${AllLinksCount}
    Comment    Create a list to store link texts
    @{LinkItems}    Create List
    Comment    Loop through all links and store links value that has length more than 1 character
    : FOR    ${INDEX}    IN RANGE    1    ${AllLinksCount}
    \    Log    ${INDEX}
    \    ${lintext}=    Get Text    xpath=(//a)[${INDEX}]
    \    Log    ${lintext}
    \    ${linklength}    Get Length    ${lintext}
    \    Run Keyword If    ${linklength}>1    Append To List    ${LinkItems}    ${lintext}
    ${LinkSize}=    Get Length    ${LinkItems}
    Log    ${LinkSize}
    Comment    Print all links
    : FOR    ${ELEMENT}    IN    @{LinkItems}
    \    Log    ${ELEMENT}
    Close Browser
