*** Settings ***
Library           Selenium2Library
Library           Collections
Resource          ../Resources/resources.robot
Library           String

*** Keywords ***
Input Username
    [Arguments]    ${username}
    Input Text    username    ${username}

Input Password
    [Arguments]    ${password}
    Input Text    password    ${password}

Submit Credentials
    Click Element    xpath=(//input)[5]

Should Be Able to Login
    Title Should Be    Toolbox | Express Oil Change & Tire Engineers

Should Not Be Able to Login
    Title Should Be    Administration | Express Oil Change & Tire Engineers

Should Be Able to Search
    Title Should Be    Express Oil Change & Tire Engineers Store Locations

Should Not Be Able to Search
    Title Should Be    Auto Service Center Near Me | Express Oil Change & Tire Engineers

Should Redirect to Shedule Appoinment Page
    Title Should Be    Schedule Appointment | Express Oil Change & Tire Engineers

Should Redirect to Careers Page
    Title Should Be    Careers | Express Oil Change & Tire Engineers

Should Redirect to Franchise Page
    Title Should Be    Express Oil Change & Tire Engineers Franchise Opportunity

Should Redirect to Cart Page
    Title Should Be    Cart | Express Oil Change & Tire Engineers

Should Redirect to Confirmation Page
    Title Should be    Express Oil Change & Tire Engineers

Should Stay in Schedule Appointment Page
    Title Should be    Schedule Appointment | Express Oil Change & Tire Engineers

Select Random Vehicle
    Go to    https://www.expressoil.com
    ${year}=    Get List Items    //*[@id="m177-year"]
    ${eval_year}=    Evaluate    random.randint(1, 37)    random
    Select From List By Label    xpath=//select[@id="m177-year"]    @{year}[${eval_year}]
    Sleep    3s
    Wait Until Element Is Visible    //*[@id="m177-make"]
    ${make}=    Get List Items    //*[@id="m177-make"]
    ${eval_make}=    Evaluate    random.randint(1, 42)    random
    Wait Until Element Is Visible    xpath=//select[@id="m177-make"]
    Select From List By Label    xpath=//select[@id="m177-make"]    @{make}[${eval_make}]
    Sleep    2s
    Wait Until Element Is Visible    xpath=//select[@id="m177-model"]
    ${model}=    Get List Items    //*[@id="m177-model"]
    Wait Until Element Is Visible    xpath=//select[@id="m177-model"]
    Select From List By Label    xpath=//select[@id="m177-model"]    @{model}[${option_value}]
    Sleep    2s
    ${option}=    Get List Items    //*[@id="m177-option"]
    Select From List By Label    xpath=//select[@id="m177-option"]    @{option}[${option_value}]
    Sleep    2s
    Click Element    xpath=//input[@name="searchvehicle"]
    Sleep    5s
    ${count}=    Get Matching Xpath Count    //a[@class="btn btn-default btn-rounded"]
    ${btnCount}=    Evaluate    random.randint(1, ${count})    random
    Click Element    xpath=(//a[@class="btn btn-default btn-rounded"])[${btnCount}]

Select Random Date
    Click Element    //*[@id="checkout-appt-date"]
    Wait Until Page Contains Element    //span[@class="ui-icon ui-icon-circle-triangle-e"]
    Click Element    //span[@class="ui-icon ui-icon-circle-triangle-e"]
    ${countDate}=    Get Matching Xpath Count    xpath=(//a[@href="#"])
    Log    ${countDate}
    ${randomDate}=    Evaluate    random.randint(2, 28)    random
    Click Link    xpath=(//a[@href="#"])[${randomDate}]

Select Random Date in Schedule Appointment
    Wait Until Page Contains Element    //*[@class="ui-state-default"]
    Click Element    //*[@class="ui-state-default"]
    ${countDate}=    Get Matching Xpath Count    xpath=(//a[@href="#"])
    Log    ${countDate}
    ${randomDate}=    Evaluate    random.randint(2, 28)    random
    Click Link    xpath=(//a[@href="#"])[${randomDate}]
